# YAML

# Unofficial documentation
* [*The YAML Format*
  ](https://symfony.com/doc/current/components/yaml/yaml_format.html)
  Symfony.com
* [*Breaking YAML Strings Over Multiple Lines*
  ](https://www.baeldung.com/yaml-multi-line)
  2020-02 Cristian Rosu
* [*YAML Multiline*
  ](https://yaml-multiline.info/)
